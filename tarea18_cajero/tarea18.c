#include <stdio.h>
#include "cajero.h"
int main(int argc, char const *argv[]) {
  menu();
  int seleccion=1;
  int deposito=0;
  int donacion=15;
  int saldo=0;
  int retiro=0;
  printf("\nSaldo: %d \n",saldo);
  while (seleccion) {
    seleccion=menu();
    switch (seleccion) {
      case 1:
      printf("\nSu saldo es: %d\n",saldo);
      break;
      case 2:
      printf("\nCantidad a depositar:");
      scanf("%d",&deposito);
      saldo=saldo+deposito;
      printf("\nSu saldo disponible es: %d\n",saldo);
      break;
      case 3:
      printf("\nSaldo disponible: %d\n",saldo);
      printf("\nCantidad a retirar:");
      scanf("%d",&retiro);
      if (retiro <= saldo) {
        saldo=saldo-retiro;
        printf("\nNuevo saldo disponible %d\n",saldo);
      }else{
        printf("\nFondos insuficientes, inserte otra cantidad.\n");
      }
      break;
      case 4:
      if (saldo>=donacion) {
        printf("\nGracias por donar.\n");
        saldo=saldo-donacion;
        printf("\nNuevo saldo disponible: %d\n",saldo);
      }else{
        printf("\nFondos insuficientes.\n");
      }
      break;
      case 5:
      printf("\nSaliendo...\n");
      return 0;
      break;
      default:
      printf("\nOpción invalida\n");
    }
  }
}
