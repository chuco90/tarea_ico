#include <stdio.h>
int main() {
  char arreglo[6];//Declaramos un arreglo de tipo char tam=6
  arreglo[0]='A';//vamos ingresando a una de las letras
  arreglo[1]='B';
  arreglo[2]='C';
  arreglo[3]='D';
  arreglo[4]='E';
  arreglo[5]='F';
  //y extraemos sus valores indicando su subindice
  printf("El valor en el subindice 0 es: %c",arreglo[0]);
  printf("\nEl valor en el subindice 1 es: %c",arreglo[1]);
  printf("\nEl valor en el subindice 2 es: %c",arreglo[2]);
  printf("\nEl valor en el subindice 3 es: %c",arreglo[3]);
  printf("\nEl valor en el subindice 4 es: %c",arreglo[4]);
  printf("\nEl valor en el subindice 5 es: %c\n",arreglo[5]);
  return 0;
}
