#include <stdio.h>

int main(int argc, char const *argv[]) {
  int var1=5;
  int var2=2;
  int resultado=0;
  //suma
  resultado=var1+var2;
  printf("La suma de %d y %d es %d\n",var1,var2,resultado);
  //resta
  resultado=var1-var2;
  printf("La resta de %d y %d es %d\n",var1,var2,resultado);
  //multiplicacion
  resultado=var1*var2;
  printf("La multiplicacion de %d y %d es %d\n",var1,var2,resultado);
  //division
  resultado=var1/var2;
  printf("La div de %d y %d es %d\n",var1,var2,resultado);
  //modulo
  resultado=var1%var2;
  printf("El modulo de %d y %d es %d\n",var1,var2,resultado);
  return 0;
}
