#include <stdio.h>
int main(int argc, char const *argv[]) {
  int var1=5;
  int var2=2;
  int resultado=0;
  printf("0 es igual a no y 1 es igual a si\n");
  //igual a ?
  resultado=var1==var2;
  printf("Es %d igual a %d? %d\n",var1,var2,resultado);
  //menor igual que?
  resultado=var1<=var2;
  printf("Es %d menor o igual a %d? %d\n",var1,var2,resultado);
  //mayor que?
  resultado=var1>var2;
  printf("Es %d mayor que %d? %d\n",var1,var2,resultado);
  //diferente de ?
  resultado=var1!=var2;
  printf("Es %d diferente de %d? %d\n",var1,var2,resultado);
  // uso de &&
  resultado=(var1>var2)&&(var1!=var2);
  printf("Es %d mayor que y diferente de %d? %d\n",var1,var2,resultado);
  return 0;
}
