#include <stdio.h>
int main(int argc, char const *argv[]) {
  //sizeof vemos cuantos bytes ocupan
  printf("Un entero ocupa:%d bytes\n",sizeof(int));
  printf("Un float ocupa:%d bytes\n",sizeof(float));
  printf("Un char ocupa:%d bytes\n",sizeof(char));
  printf("Un entero largo ocupa:%d bytes\n",sizeof(long int));
  return 0;
}
