#include <stdio.h>
#define menudeo 450
#define mayoreo 435
int main(int argc, char const *argv[]) {
  int piezas=0;
  float res1=0,res2=0,res3=0,res4=0;
  printf("Ingrese numero de piezas vendidas:");
  scanf("%d",&piezas);//escaneamos numero de piezas
  if (piezas<=12) {
    res1=((piezas*menudeo)*.05);
    printf("Tu comision es: %.2f\n",res1);
  }
  else if (piezas>=13&&piezas<=23) {
    res2=((piezas*menudeo)*.07);
    printf("Tu comision es: %.2f\n",res2);
  }
  else if (piezas>=24&&piezas<=30) {
    res3=((piezas*mayoreo)*.07);
    printf("Tu comision es: %.2f\n",res3);
  }
  else if (piezas>30) {
    res4=((piezas*mayoreo)*.10);
    printf("Tu comision es: %.2f\n",res4);
  }
  return 0;
}
